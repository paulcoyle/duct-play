(ns duct-play.http
  (:require [duct.logger :as logger]
            [integrant.core :as ig]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]))

(defn response [status body & {:as headers}]
  {:status status :body body :headers headers})
            
(def ok (partial response 200))

(def echo
  {:name :echo
   :enter
   (fn [context]
     (let [request (:request context)]
          [response (ok context)]
      (assoc context :response response)))})

(def routes
  (route/expand-routes
    #{["/" :get echo :route-name :root]}))

(defmethod ig/init-key ::service [_ {:keys [port]}]
  {::http/routes routes
   ::http/type :jetty
   ::http/port port
   ::http/join? false})

(defmethod ig/init-key ::server [_ {:keys [service logger]}] 
  (logger/log logger :report ::starting-server (::http/port service))
  {:server (http/start (http/create-server service))
   :logger logger})

(defmethod ig/halt-key! ::server [_ {:keys [server logger]}]
  (logger/log logger :report ::halting-server)
  (when server (http/stop server)))
